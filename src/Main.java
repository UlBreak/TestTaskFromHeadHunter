import java.util.ArrayList;
import java.util.Scanner;


public class Main {


    public static void main(String[] args) {
        int firstStackPaperSize, secondStackPaperSize, acceptableAmount, interimAmount, maxResumes = 0, resumes = 0;
        Scanner scanner = new Scanner(System.in);

        firstStackPaperSize = scanner.nextInt();
        secondStackPaperSize = scanner.nextInt();
        acceptableAmount = scanner.nextInt();


        ArrayList<String> firstStackPaper = new ArrayList<>();
        ArrayList<String> secondStackPaper = new ArrayList<>();

        for (int i = 0; i < Math.max(firstStackPaperSize, secondStackPaperSize); i++) {
            String first = scanner.next();
            String second = scanner.next();
            firstStackPaper.add(first);
            secondStackPaper.add(second);
        }


        interimAmount = 0;


        int i = 0;
        while ((interimAmount < acceptableAmount) && (i < firstStackPaperSize)) {
            if ((interimAmount + Integer.parseInt(firstStackPaper.get(i))) <= acceptableAmount) {
                interimAmount += Integer.parseInt(firstStackPaper.get(i));
                resumes++;
            } else break;
            i++;


        }

        maxResumes = resumes;
        if(i>0)
            i = i - 1;

        for (int j = 0; j < secondStackPaperSize; j++) {
            if ((interimAmount + Integer.parseInt(secondStackPaper.get(j))) <= acceptableAmount) {
                if ((interimAmount + Integer.parseInt(secondStackPaper.get(j))) <= acceptableAmount) {
                    interimAmount += Integer.parseInt(secondStackPaper.get(j));
                    resumes++;
                }

            }

            else {
                while (((Integer.parseInt(secondStackPaper.get(j)) + interimAmount) >= acceptableAmount) && (i > 0)) {

                    interimAmount -= Integer.parseInt(firstStackPaper.get(i));
                    i--;
                    resumes--;
                }
                if ((interimAmount + Integer.parseInt(secondStackPaper.get(j))) <= acceptableAmount) {
                    interimAmount += Integer.parseInt(secondStackPaper.get(j));
                    resumes++;
                }
                else break;
            }
            if (resumes > maxResumes) {
                maxResumes = resumes;
            }
        }
        System.out.println(maxResumes);
    }
}



